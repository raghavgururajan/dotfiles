(in-package :stumpwm)

;; Set prefix to Super+T.
(set-prefix-key (kbd "s-t"))

;; Set font to NotoMono.
(require :ttf-fonts)
(setf xft:*font-dirs* '("/run/current-system/profile/share/fonts/"))
(setf clx-truetype:+font-cache-filename+ (concat (getenv "HOME") "/.fonts/font-cache.sexp"))
(xft:cache-fonts)
(set-font (make-instance 'xft:font :family "FreeMono" :subfamily "Regular" :size 12))
