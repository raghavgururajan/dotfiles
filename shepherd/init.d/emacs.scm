(define emacs
  (make <service>
    #:provides '(emacs)
    #:start (make-system-constructor "emacs --daemon")
    #:stop (make-system-destructor "emacsclient -e '(kill-emacs)'")
    #:respawn? #t))

(register-services emacs)

(start emacs)

