(define gpg-agent
  (make <service>
    #:provides '(gpg-agent)
    #:start (make-system-constructor "gpg-agent --daemon")
    #:stop (make-system-destructor "gpg-connect-agent killagent /bye")
    #:respawn? #t))

(register-services gpg-agent)

(start gpg-agent)

