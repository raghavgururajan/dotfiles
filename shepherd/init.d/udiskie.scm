(define udiskie
  (make <service>
    #:provides '(udiskie)
    #:start (make-system-constructor "udiskie &")
    #:stop (make-system-destructor "pkill -9 udiskie")
    #:respawn? #t))

(register-services udiskie)

(start udiskie)

