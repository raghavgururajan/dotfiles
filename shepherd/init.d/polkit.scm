(define polkit-agent
  (make <service>
    #:provides '(polkit-agent)
    #:start (make-system-constructor "$HOME/.guix-profile/libexec/polkit-gnome-authentication-agent-1 &")
    #:stop (make-system-destructor "pkill -9 polkit-gnome-authentication-agent-1")
    #:respawn? #t))

(register-services polkit-agent)

(start polkit-agent)

