;; Coding System
(prefer-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(set-file-name-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-next-selection-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)

;; Font
(set-fontset-font "fontset-default" 'unicode "Noto Sans Mono")
(set-fontset-font "fontset-startup" 'unicode "Noto Sans Mono")
(set-fontset-font "fontset-default" 'symbol "Noto Color Emoji")
(set-fontset-font "fontset-startup" 'symbol "Noto Color Emoji")

;; Language
(set-language-environment "UTF-8")

